<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\Qualiteam\VideoTag;

abstract class Main extends \XLite\Module\AModule
{

	public static function getAuthorName()
	{
		return 'Qualiteam';
	}
	
	public static function getModuleName()
	{
		return 'Video tag import';
	}
	
	public static function getMajorVersion()
	{
		return '5.3';
	}
	
	public static function getMinorVersion()
	{
		return '2';
	}

    public static function getBuildVersion()
    {
        return '0';
    }
		
	public static function getDescription()
	{
		return 'Custom development. The module prevents removing defined tags from the product description during the import process';
	}
	
	public static function showSettingsForm() 
	{
		return true;
	}
}
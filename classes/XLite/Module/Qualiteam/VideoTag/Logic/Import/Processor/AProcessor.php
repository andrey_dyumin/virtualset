<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\Qualiteam\VideoTag\Logic\Import\Processor;

abstract class AProcessor extends XLite\Logic\Import\Processor\AProcessor implements \XLite\Base\IDecorator
{

	protected function replaceWithCodes($value, &$replaced_codes) {
	
		$tags = \XLite\Core\Config::getInstance()->Qualiteam->VideoTag->tags;
    	$tags_array = explode(",", $tags);
    	
    	$replaced_codes = array();
    	$matches = array();
    	
    	foreach ($tags_array as $tag) {
    		$tag = trim($tag);

    		do {
    			// for pair tags
    			//
	    		if (preg_match("/(.*)<" . quotemeta($tag) . "(.*)<\/" . quotemeta($tag) . ">(.*)/si", $value, $matches)) {
	    			$code = 'tag' . md5(microtime().rand());
	    			
	    			$value = $matches[1] . $code . $matches[3];
	    			$replaced_codes[$code] = "<" . $tag . $matches[2] . "</" . $tag . ">";
	    			continue;
	    		}

	    		// for single tags. It is not a universal solution, but we do not expect to use it anyway.
	    		//
	    		if (preg_match("/(.*)<" . quotemeta($tag) . "([^<]*)\/>(.*)/si", $value, $matches)) {
	    			$code = 'tag' . md5(microtime().rand());
	    			
	    			$value = $matches[1] . $code . $matches[3];
	    			$replaced_codes[$code] = "<" . $tag . $matches[2] . "/>";
	    			continue;
	    		}
	    			    		
	    	} while (false);
	    }
	    
	    return $value;
	}
        

	protected function getPurifiedValue(array $column, $value) {

        if (is_array($value)) {
        
            foreach ($value as $k => $v) {
                $value[$k] = $this->getPurifiedValue($column, $v);
            }
            
		} else {

			$replaced_codes = array();		
			if ($column['name'] == 'description') {
				$value = $this->replaceWithCodes($value, $replaced_codes);
			}

			$value = parent::getPurifiedValue($column, $value);
			
			if ($column['name'] == 'description') {
				if (!empty($replaced_codes)) {
					foreach ($replaced_codes as $code => $text) {
						$value = str_replace($code, $text, $value);
					}
				}
			}
		}

		return $value;		
	} 
}